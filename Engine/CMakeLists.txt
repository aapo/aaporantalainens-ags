# alogg.c is from
# http://replay.web.archive.org/20060518092445/http://www.hero6.com/filereviver/alogg.zip
# Which is a shared library available at least on Ark Linux, Fedora and
# openSUSE - but the version included here provides 2 extra functions
# (alogg_is_end_of_oggstream(), alogg_is_end_of_ogg()), so the external lib
# can't be used
set(SOURCES acaudio.cpp acfonts.cpp acchars.cpp acgfx.cpp misc.cpp ac.cpp
	acgui.cpp routefnd.cpp acplatfm.cpp acsound.cpp ali3dsw.cpp scrptrt.cpp
	acdialog.cpp bigend.cpp alogg.c almp3.c
	${COMMON}/cscommon.cpp ${COMMON}/csrun.cpp ${COMMON}/sprcache.cpp
	${COMMON}/lzw.cpp ${COMMON}/compress.cpp
	libsrc/allegro-4.2.2-agspatch/file.c
	libsrc/aastr-0.1.1/aastr.c libsrc/aastr-0.1.1/aautil.c
	libsrc/apeg-1.2.1/display.c libsrc/apeg-1.2.1/getbits.c libsrc/apeg-1.2.1/getblk.c
	libsrc/apeg-1.2.1/gethdr.c libsrc/apeg-1.2.1/getpic.c libsrc/apeg-1.2.1/idct.c
	libsrc/apeg-1.2.1/motion.c libsrc/apeg-1.2.1/mpeg1dec.c libsrc/apeg-1.2.1/ogg.c
	libsrc/apeg-1.2.1/recon.c libsrc/apeg-1.2.1/audio/apegcommon.c
	libsrc/apeg-1.2.1/audio/audio.c libsrc/apeg-1.2.1/audio/mpg123.c
	libsrc/hq2x/hq2x3x.cpp
)
# This affects APEG only. MPEG Audio is still supported through almp3.
# If we ever remove DISABLE_MPEG_AUDIO, we also have to add the following
# source files:
# libsrc/apeg-1.2.1/audio/dct64.c libsrc/apeg-1.2.1/audio/decode_1to1.c
# libsrc/apeg-1.2.1/audio/decode_2to1.c libsrc/apeg-1.2.1/audio/decode_4to1.c
# libsrc/apeg-1.2.1/audio/layer1.c libsrc/apeg-1.2.1/audio/layer2.c
# libsrc/apeg-1.2.1/audio/layer3.c libsrc/apeg-1.2.1/audio/readers.c
# libsrc/apeg-1.2.1/audio/tabinit.c libsrc/apeg-1.2.1/audio/vbrhead.c
add_definitions(-DDISABLE_MPEG_AUDIO)
if(APPLE)
	add_definitions(-DMAC_VERSION)
	set(SOURCES ${SOURCES} macport.cpp acplmac.cpp ${COMMON}/mousew32.cpp ${COMMON}/Clib32.cpp)
# UNIX is also set for OSX - so it must be checked for
# after checking for APPLE
elseif(UNIX)
	add_definitions(-DLINUX_VERSION)
	if(NOT ${CMAKE_SYSTEM_NAME} MATCHES "Linux")
		add_definitions(-DBSD_VERSION)
	endif()
	# There's thousands of those - let's not hide the errors
	# in the warning spewage for now
	add_definitions(-Wno-write-strings)
	set(SOURCES ${SOURCES} acpllnx.cpp ${COMMON}/mousew32.cpp ${COMMON}/Clib32.cpp)
elseif(WIN32)
	add_definitions(-DWINDOWS_VERSION)
	set(SOURCES ${SOURCES} acplwin.cpp acwavi.cpp ali3dd3d.cpp acwavi3d.cpp acdebug.cpp ${COMMON}/mousew32.cpp ${COMMON}/Clib32.cpp)
	add_executable(acwsetup acwsetup.cpp)
else()
	add_definitions(-DDOS_VERSION)
	set(SOURCES ${SOURCES} acpldos.cpp ${COMMON}/mouse32.cpp ${COMMON}/Clib32.cpp)
endif()

add_executable(ags ${SOURCES})
target_link_libraries(ags ${ALLEGRO_LIBRARY} ${DUMB_LIBRARY} ${ALDUMB_LIBRARY} ${LIBCDA_LIBRARY} ${ALFONT_LIBRARY} ${THEORA_LIBRARY} ${OGG_LIBRARY} ${VORBIS_LIBRARY} ${VORBISFILE_LIBRARY} ${MPG123_LIBRARY})
install(TARGETS ags DESTINATION bin)
